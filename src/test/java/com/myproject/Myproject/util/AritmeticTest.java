package com.myproject.Myproject.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AritmeticTest {

    Aritmetic aritmetic = new Aritmetic();
    @Test
    void add() {
        Assertions.assertEquals(3, aritmetic.add(1,2));
    }

    @Test
    void sub() {

        Assertions.assertEquals(2, aritmetic.sub(4,2));
    }

    @Test
    void mul() {

        Assertions.assertEquals(8, aritmetic.mul(4,2));
    }

    @Test
    void div() {
        Assertions.assertEquals(2, aritmetic.div(4,2));
    }
}